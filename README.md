# Fancy Search

Search plain text files with boolean operators using human-ish syntax.

## What. Why? Haven't you heard of grep/awk/rg/ag? Isn't this slow?

The frontends I've used for my media library don't let me do complicated searches.
They always have limitations on specific fields, or no support for boolean patterns.

Writing a regex pattern every time I wanna do a fancy query is a pretty
poor user experience. It's also quite easy to write a query which only matches
a subset of what you intended because some queries require engaging brain.

With a more human friendly syntax, I don't have to think too hard.
My searches are small enough and happen so infrequently that four seconds
is not disruptive.

Below are some unscientific benchmarks on an averagely large directory
on a networked file share.

```console
$ ls -1 | wc -l
835

$ time ls -l
ls --color=tty -l  0.00s user 0.00s system 23% cpu 0.040 total

$ time grep "Action" */*.nfo
grep --color=auto --exclude-dir={.bzr,CVS,.git,.hg,.svn,.idea,.tox}  */*.nfo  0.04s user 0.30s system 11% cpu 2.826 total

$ time fs . "Action"
fs . "Action"  0.75s user 0.37s system 28% cpu 3.927 total
```

More complicated `fs` queries do not seem to have an impact on performance.

## Installation

```shell
poetry install
poetry build
pip install dist/*whl
```

## Usage

```text
$ fs -h
usage: fs [-h] [--extension EXTENSION] directory pattern

positional arguments:
  directory             The directory to search
  pattern               The search pattern

options:
  -h, --help            show this help message and exit
  --extension EXTENSION
                        File extension to query (default: '.nfo')
```

Searching `.nfo` files for "Science Fiction" which is also "Action" or "Adventure":

```shell
fs . "(Action OR Adventure) AND Science Fiction"
```

Defaults to searching files ending in `.nfo` (for Kodi media libraries).
This can be overridden with the `--extension` parameter.

Searching `.md` files for "Migrat" (as in, Migration, Migrate, Migrating, etc)
and also either Postgres or MySQL:

```shell
fs . --extension ".md" "(MySQL or Postgres) AND Migrat"
```

Search is powered by `eldar`. For more query examples, see upstream docs:
<https://github.com/kerighan/eldar>

## Future Improvements

I spent about two hours on this before I found `eldar`, at which point I
threw out my crappy query parser, and lobbed in some jank OS munging.

On the off chance I use this enough to revisit it. A few good starts:

* Indexing searches locally on faster flash memory would probably
  make the greatest speed increase. Individually reading hundreds of files
  on a networked filesystem has a lot of IO overhead.
* Rewriting in Rust or Go would probably speed things up as well,
  although I suspect not as much as turning 800+ `f.read()`s into just one!
