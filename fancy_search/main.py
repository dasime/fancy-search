import argparse
import os
import sys

import eldar


def search_files(directory, file_extension, pattern):
    query = eldar.Query(
        pattern, ignore_case=True, ignore_accent=True, match_word=False
    )

    print(f'Detected pattern: "${query}"', file=sys.stderr)

    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(file_extension):
                with open(os.path.join(root, file), "r") as f:
                    contents = f.read()
                    if query(contents):
                        print(os.path.join(root, file))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", help="The directory to search")
    parser.add_argument("pattern", help="The search pattern")
    parser.add_argument(
        "--extension",
        help="File extension to query (default: '.nfo')",
        default=".nfo",
    )
    args = parser.parse_args()

    search_files(args.directory, args.extension, args.pattern)


if __name__ == "__main__":
    main()
